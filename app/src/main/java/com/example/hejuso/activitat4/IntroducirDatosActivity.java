package com.example.hejuso.activitat4;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class IntroducirDatosActivity extends AppCompatActivity {

    private MyDBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introducir_datos);
        
        dbAdapter = new MyDBAdapter(this);
        dbAdapter.open();

        Button send_alumno = findViewById(R.id.enviar_alumno);
        Button send_profesor = findViewById(R.id.enviar_profesor);

        Button delete_alumno = findViewById(R.id.enviar_alumno_id);
        Button delete_profesor = findViewById(R.id.enviar_profesor_id);

        Button delete_db = findViewById(R.id.borrar_bbdd);

        send_alumno.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextInputEditText nombre_alumno = findViewById(R.id.nombre);
                TextInputEditText edad_alumno = findViewById(R.id.edad);
                TextInputEditText ciclo_alumno = findViewById(R.id.ciclo);
                TextInputEditText curso_alumno = findViewById(R.id.curso);

                TextInputEditText nota_media = findViewById(R.id.notamedia);

                try {
                    String nombre_alumno_str = nombre_alumno.getText().toString();
                    int edad_alumno_str = Integer.parseInt(edad_alumno.getText().toString());
                    String ciclo_alumno_str = ciclo_alumno.getText().toString();
                    String curso_alumno_str = curso_alumno.getText().toString();
                    String nota_media_str = nota_media.getText().toString();

                    dbAdapter.insertarAlumno(nombre_alumno_str, edad_alumno_str,ciclo_alumno_str,curso_alumno_str,nota_media_str);

                    Toast.makeText(getApplicationContext(), "Se ha añadido a un alumno", 500).show();

                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "La edad no es un numero correcto", 500).show();
                }
            }
        });

        send_profesor.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TextInputEditText nombre_profesor = findViewById(R.id.prof_nombre);
                TextInputEditText edad_profesor = findViewById(R.id.prof_edad);
                TextInputEditText ciclo_profesor = findViewById(R.id.prof_ciclo);
                TextInputEditText curso_profesor = findViewById(R.id.curso_prof);
                TextInputEditText despacho = findViewById(R.id.prof_despacho);


                try {
                    String nombre_profesor_str = nombre_profesor.getText().toString();
                    int edad_profesor_str = Integer.parseInt(edad_profesor.getText().toString());
                    String ciclo_profesor_str = ciclo_profesor.getText().toString();
                    String curso_profesor_str = curso_profesor.getText().toString();

                    String despacho_str = despacho.getText().toString();

                    dbAdapter.insertarProfesor(nombre_profesor_str, edad_profesor_str, ciclo_profesor_str,curso_profesor_str, despacho_str);
                    Toast.makeText(getApplicationContext(), "Se ha añadido a un profesor", 500).show();

                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "La edad no es un numero correcto", 500).show();
                }


            }

        });

        delete_profesor.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TextInputEditText id_profesor = findViewById(R.id.id_profesor);

                try {
                    int id_int = Integer.parseInt(id_profesor.getText().toString());
                    dbAdapter.borrarProfesor(id_int);
                    Toast.makeText(getApplicationContext(), "Se ha eliminado un profesor", 500).show();
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Introduzca un ID correcto", 500).show();
                }


            }

        });

        delete_alumno.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TextInputEditText id_alumno = findViewById(R.id.id_alumno);

                try {
                    int id_int = Integer.parseInt(id_alumno.getText().toString());
                    dbAdapter.borrarAlumno(id_int);
                    Toast.makeText(getApplicationContext(), "Se ha eliminado un alumno", 500).show();

                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Introduzca un ID correcto", 500).show();
                }


            }

        });

        delete_db.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dbAdapter.borrarDB();
                Toast.makeText(getApplicationContext(), "Base de datos eliminada", 500).show();
            }
        });

    }
}
