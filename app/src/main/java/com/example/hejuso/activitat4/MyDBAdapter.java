package com.example.hejuso.activitat4;

/**
 * Created by hejuso on 10/12/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class MyDBAdapter {

    // Definiciones y constantes
    private static final String DATABASE_NAME = "dbdprofesores.db";


    private static final int DATABASE_VERSION = 1;

    private static final String NOMBRE = "nombre";
    private static final String EDAD = "edad";
    private static final String CICLO = "ciclo";
    private static final String CURSO = "curso";

    private static final String NOTA_MEDIA = "nota_media";
    private static final String DESPACHO = "despacho";

    private static final String DATABASE_CREATE_ALUMNOS = "CREATE TABLE alumnos (_id integer primary key autoincrement, nombre text, edad integer, ciclo text, curso text, nota_media text);";
    private static final String DATABASE_CREATE_PROFESORES = "CREATE TABLE profesores (_id integer primary key autoincrement, nombre text, edad integer, ciclo text, curso text, despacho text);";

    private static final String DATABASE_DROP_PROF = "DROP TABLE IF EXISTS profesores;";
    private static final String DATABASE_DROP_AL = "DROP TABLE IF EXISTS alumnos;";

    // Contexto de la aplicación que usa la base de datos
    private final Context context;
    // Clase SQLiteOpenHelper para crear/actualizar la base de datos
    private MyDbHelper dbHelper;
    // Instancia de la base de datos
    private SQLiteDatabase db;

    public MyDBAdapter (Context c){
        context = c;
        dbHelper = new MyDbHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        //OJO open();
    }

    public void open(){

        try{
            db = dbHelper.getWritableDatabase();
        }catch(SQLiteException e){
            db = dbHelper.getReadableDatabase();
        }

    }


    public void insertarAlumno(String nombre, int edad, String ciclo, String curso, String nota_media){
        String DATABASE_TABLE = "alumnos";

        //Creamos un nuevo registro de valores a insertar
        ContentValues newValues = new ContentValues();
        //Asignamos los valores de cada campo
        newValues.put(NOMBRE,nombre);
        newValues.put(EDAD,edad);
        newValues.put(CICLO,ciclo);
        newValues.put(CURSO,curso);
        newValues.put(NOTA_MEDIA,nota_media);
        db.insert(DATABASE_TABLE,null,newValues);
    }

    public void insertarProfesor(String nombre, int edad, String ciclo, String curso, String despacho){
        String DATABASE_TABLE = "profesores";

        //Creamos un nuevo registro de valores a insertar
        ContentValues newValues = new ContentValues();
        //Asignamos los valores de cada campo
        newValues.put(NOMBRE,nombre);
        newValues.put(EDAD,edad);
        newValues.put(CICLO,ciclo);
        newValues.put(CURSO,curso);

        newValues.put(DESPACHO,despacho);

        db.insert(DATABASE_TABLE,null,newValues);
    }

    public void borrarProfesor(int id){
        String DATABASE_TABLE = "profesores";

        db.delete(DATABASE_TABLE,"_id='"+id+"'",null);
    }

    public void borrarAlumno(int id){
        String DATABASE_TABLE = "alumnos";

        db.delete(DATABASE_TABLE,"_id='"+id+"'",null);
    }

    public void borrarDB(){
        context.deleteDatabase(DATABASE_NAME);
     }

     // FILTROS
    public ArrayList<String> recuperarTodosAlumnos(){

        String DATABASE_TABLE = "alumnos";

        ArrayList<String> alumnos = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,null,null,null,null,null,null);
        //Recorremos el cursor
        if (cursor != null && cursor.moveToFirst()){
            do{
                alumnos.add(cursor.getString(1)+" "+cursor.getString(2)+" "+cursor.getString(3)+" "+cursor.getString(4)+" "+cursor.getString(5));
            }while (cursor.moveToNext());
        }

        return alumnos;
    }

    public ArrayList<String> recuperarTodosProfesores(){

        String DATABASE_TABLE = "profesores";

        ArrayList<String> alumnos = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,null,null,null,null,null,null);
        //Recorremos el cursor
        if (cursor != null && cursor.moveToFirst()){
            do{
                alumnos.add(cursor.getString(1)+" "+cursor.getString(2)+" "+cursor.getString(3)+" "+cursor.getString(4)+" "+cursor.getString(5));
            }while (cursor.moveToNext());
        }

        return alumnos;
    }

    public ArrayList<String> recuperarTodosProfesoresYAlumnos(){

        String DATABASE_TABLE = "'alumnos','profesores'";

        ArrayList<String> alumnos = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.rawQuery("select * from alumnos,profesores" ,null,null);
        //Recorremos el cursor
        if (cursor != null && cursor.moveToFirst()){
            do{
                alumnos.add(cursor.getString(1)+" "+cursor.getString(2)+" "+cursor.getString(3)+" "+cursor.getString(4)+" "+cursor.getString(5));
            }while (cursor.moveToNext());
        }

        return alumnos;
    }

    public ArrayList<String> recuperarAlumnosCiclos(){

        String DATABASE_TABLE = "alumnos";

        ArrayList<String> alumnos = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,new String[]{"nombre","ciclo"},null,null,null,null,null);

        //Recorremos el cursor y solo muestra el ciclo
        if (cursor != null && cursor.moveToFirst()){
            do{
                alumnos.add(cursor.getString(0)+" "+cursor.getString(1));
            }while (cursor.moveToNext());
        }

        return alumnos;
    }

    public ArrayList<String> recuperarProfesoresCiclos(){

        String DATABASE_TABLE = "profesores";

        ArrayList<String> profesores = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,new String[]{"nombre","ciclo"},null,null,null,null,null);

        //Recorremos el cursor y solo muestra el ciclo
        if (cursor != null && cursor.moveToFirst()){
            do{
                profesores.add(cursor.getString(0)+" "+cursor.getString(1));
            }while (cursor.moveToNext());
        }

        return profesores;
    }

    public ArrayList<String> recuperarAlumnosCursos(){

        String DATABASE_TABLE = "alumnos";

        ArrayList<String> alumnos = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,new String[]{"nombre","curso"},null,null,null,null,null);

        //Recorremos el cursor y solo muestra el curso
        if (cursor != null && cursor.moveToFirst()){
            do{
                alumnos.add(cursor.getString(0)+" "+cursor.getString(1));
            }while (cursor.moveToNext());
        }

        return alumnos;
    }

    public ArrayList<String> recuperarProfesoresCursos(){

        String DATABASE_TABLE = "profesores";

        ArrayList<String> profesores = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,new String[]{"nombre","curso"},null,null,null,null,null);

        //Recorremos el cursor y solo muestra el curso
        if (cursor != null && cursor.moveToFirst()){
            do{
                profesores.add(cursor.getString(0)+" "+cursor.getString(1));
            }while (cursor.moveToNext());
        }

        return profesores;
    }

    public ArrayList<String> recuperarAlumnosCicloYCurso(){

        String DATABASE_TABLE = "alumnos";

        ArrayList<String> alumnos = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,new String[]{"nombre","ciclo","curso"},null,null,null,null,null);

        //Recorremos el cursor y solo muestra el curso
        if (cursor != null && cursor.moveToFirst()){
            do{
                alumnos.add(cursor.getString(0)+" "+cursor.getString(1)+" "+cursor.getString(2));
            }while (cursor.moveToNext());
        }

        return alumnos;
    }

    public ArrayList<String> recuperarProfesoresCicloYCurso(){

        String DATABASE_TABLE = "profesores";

        ArrayList<String> profesores = new ArrayList<String>();
        //Recuperamos en un cursor la consulta realizada
        Cursor cursor = db.query(DATABASE_TABLE,new String[]{"nombre","ciclo","curso"},null,null,null,null,null);

        //Recorremos el cursor y solo muestra el curso
        if (cursor != null && cursor.moveToFirst()){
            do{
                profesores.add(cursor.getString(0)+" "+cursor.getString(1)+" "+cursor.getString(2));
            }while (cursor.moveToNext());
        }

        return profesores;
    }

    private static class MyDbHelper extends SQLiteOpenHelper {

        public MyDbHelper (Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
            super(context,name,factory,version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE_ALUMNOS);
            db.execSQL(DATABASE_CREATE_PROFESORES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DATABASE_DROP_PROF);
            db.execSQL(DATABASE_DROP_AL);
            onCreate(db);
        }

    }
}