package com.example.hejuso.activitat4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecibirDatosActivity extends AppCompatActivity {
    ListView listView;
    private MyDBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_datos);

        dbAdapter = new MyDBAdapter(this);
        dbAdapter.open();

        // Recogemos el ListView de la vista
        listView = findViewById(R.id.datos_recogidos);

        //RECUPERAR ALUMNOS

        Button recuperar_alumnos = findViewById(R.id.todos_est);

        recuperar_alumnos.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                ArrayList<String> alumnos = dbAdapter.recuperarTodosAlumnos();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,alumnos);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        Button recuperar_alumnos_ciclo = findViewById(R.id.est_ciclo);

        recuperar_alumnos_ciclo.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

//               EditText busqueda = findViewById(R.id.busqueda);
//               String busqueda_str = busqueda.getText().toString();

                ArrayList<String> alumnos = dbAdapter.recuperarAlumnosCiclos();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,alumnos);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos por ciclo", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        Button recuperar_alumnos_curso = findViewById(R.id.est_curso);

        recuperar_alumnos_curso.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

//               EditText busqueda = findViewById(R.id.busqueda);
//               String busqueda_str = busqueda.getText().toString();

                ArrayList<String> alumnos = dbAdapter.recuperarAlumnosCursos();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,alumnos);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos por ciclo", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        Button recuperar_alumnos_cursoyciclo = findViewById(R.id.est_ciclo_curso);

        recuperar_alumnos_cursoyciclo.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

//               EditText busqueda = findViewById(R.id.busqueda);
//               String busqueda_str = busqueda.getText().toString();

                ArrayList<String> alumnos = dbAdapter.recuperarAlumnosCicloYCurso();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,alumnos);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos por ciclo", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        //RECUPERAR PROFESORES

        Button recuperar_profesores = findViewById(R.id.todos_prof);

        recuperar_profesores.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                ArrayList<String> profesores = dbAdapter.recuperarTodosProfesores();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,profesores);

                Toast.makeText(getApplicationContext(), "Recuperados todos los profesores", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        Button recuperar_profesores_ciclo = findViewById(R.id.prof_ciclo);

        recuperar_profesores_ciclo.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

//               EditText busqueda = findViewById(R.id.busqueda);
//               String busqueda_str = busqueda.getText().toString();

                ArrayList<String> profesores = dbAdapter.recuperarProfesoresCiclos();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,profesores);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos por ciclo", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        Button recuperar_profesores_curso = findViewById(R.id.prof_curso);

        recuperar_profesores_curso.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

//               EditText busqueda = findViewById(R.id.busqueda);
//               String busqueda_str = busqueda.getText().toString();

                ArrayList<String> profesores = dbAdapter.recuperarProfesoresCursos();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,profesores);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos por ciclo", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        Button recuperar_profesores_cursoyciclo = findViewById(R.id.prof_ciclo_curso);

        recuperar_profesores_cursoyciclo.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

//               EditText busqueda = findViewById(R.id.busqueda);
//               String busqueda_str = busqueda.getText().toString();

                ArrayList<String> profesores = dbAdapter.recuperarProfesoresCicloYCurso();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,profesores);

                Toast.makeText(getApplicationContext(), "Recuperados todos los alumnos por ciclo", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

        // RECUPERAR PROFESORES Y ALUMNOS

        Button recuperar_profesores_alumnos = findViewById(R.id.todos_prof_al);

        recuperar_profesores_alumnos.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                ArrayList<String> profesores_al = dbAdapter.recuperarTodosProfesoresYAlumnos();

                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,profesores_al);

                Toast.makeText(getApplicationContext(), "Recuperados todos los profesores", 500).show();

                // Le asignamos el adaptador a la vista ListView
                listView.setAdapter(arrayAdapter);

            }
        });

    }
}
